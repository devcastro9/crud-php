<?php include './plantillas/head.php';?>
    <div class="container">
        <div class="row" style="padding:15px;">
            <div class="col" style="padding:15px;">
                <div class="card border-primary" style="background-color: white;">
                    <img class="card-img-top" src="holder.js/100x180/" alt="">
                    <div class="card-body">
                        <a href="materia/index.php"><h4 class="card-title text-primary">Materia</h4></a>
                        <p class="card-text">Base de datos de Materia (completado)</p>
                    </div>
                </div>
            </div>
            <div class="col" style="padding:15px;">
                <div class="card border-primary" style="background-color: white;">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <a href="aula/index.php"><h4 class="card-title text-primary">Aula</h4></a>
                    <p class="card-text">Base de datos de Aula (completado)</p>
                </div>
                </div>
            </div>
            <div class="col" style="padding:15px;">
                <div class="card border-primary" style="background-color: white;">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <a href="turno/index.php"><h4 class="card-title text-primary">Turno</h4></a>
                    <p class="card-text">Base de datos de Turno (completado)</p>
                </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding:15px;">
            <div class="col" style="padding:15px;">
                <div class="card border-primary" style="background-color: white;">
                    <img class="card-img-top" src="holder.js/100x180/" alt="">
                    <div class="card-body">
                        <a href="alumno/index.php"><h4 class="card-title text-primary">Alumno</h4></a>
                        <p class="card-text">Base de datos de Alumno (completado)</p>
                    </div>
                </div>
            </div>
            <div class="col" style="padding:15px;">
                <div class="card border-primary" style="background-color: white;">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <a href="profesor/index.php"><h4 class="card-title text-primary">Profesor</h4></a>
                    <p class="card-text">Base de datos de Profesor (completado)</p>
                </div>
                </div>
            </div>
            <div class="col" style="padding:15px;">
                <div class="card border-primary" style="background-color: white;">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <a href="curso/index.php"><h4 class="card-title text-primary">Curso</h4></a>
                    <p class="card-text">Base de datos de Curso</p>
                </div>
                </div>
            </div>
        </div>
    </div>
<?php include './plantillas/foot.php';?>