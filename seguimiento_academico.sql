-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 12-12-2019 a las 04:35:29
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `seguimiento_academico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `idalumno` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(40) NOT NULL,
  `ci` varchar(15) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `genero` char(1) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `matricula` int(11) NOT NULL,
  `fecha_inscripcion` date NOT NULL,
  `pagosaldia` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`idalumno`, `nombre`, `apellido`, `ci`, `fecha_nacimiento`, `genero`, `celular`, `correo_electronico`, `matricula`, `fecha_inscripcion`, `pagosaldia`) VALUES
(1, 'Juan Jose', 'Mamani', '8522145LP', '1989-08-03', 'O', '78763833', 'opo89@gmail.com', 56677, '2019-12-18', 'N'),
(2, 'Donald', 'Lake', '5185185SCZ', '1998-09-02', 'M', '78763833', 'opo90@gmail.com', 32144, '2019-12-25', 'N'),
(3, 'Orlando', 'Castro', '6852898', '1996-04-29', 'M', '78763833', 'jeffdoom6@gmail.com', 1728680, '2019-12-11', 'S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aula`
--

CREATE TABLE `aula` (
  `idaula` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `codigo` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aula`
--

INSERT INTO `aula` (`idaula`, `descripcion`, `codigo`) VALUES
(1, 'Aula J1*', 'J1'),
(2, 'Aula D2', 'D2'),
(4, 'Aula G2', 'G2'),
(5, 'Aula H1', 'H1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `idcurso` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `idprofesor` int(11) NOT NULL,
  `idturno` int(11) NOT NULL,
  `idaula` int(11) NOT NULL,
  `idmateria` int(11) NOT NULL,
  `gestion` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`idcurso`, `idalumno`, `idprofesor`, `idturno`, `idaula`, `idmateria`, `gestion`) VALUES
(1, 1, 3, 3, 2, 3, 2019),
(2, 2, 3, 3, 2, 3, 2019),
(3, 5, 3, 3, 2, 3, 2019);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `idmateria` int(11) NOT NULL,
  `materia` varchar(50) NOT NULL,
  `codigomateria` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`idmateria`, `materia`, `codigomateria`) VALUES
(1, 'Redes neuronales', 'IN101'),
(2, 'Informatica', 'IF101'),
(3, 'Desarrollo web', 'NF206'),
(4, 'Materia', '129'),
(6, 'Web prog', 'W123'),
(8, 'test', '321');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `idprofesor` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(40) NOT NULL,
  `ci` varchar(15) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `genero` char(1) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `fecha_contrato` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`idprofesor`, `nombre`, `apellido`, `ci`, `fecha_nacimiento`, `genero`, `celular`, `correo_electronico`, `fecha_contrato`) VALUES
(1, 'Orlando', 'Castro12', '1234156LP', '1969-12-22', 'M', '123456', 'jekljfsad@gmail.com', '2019-12-11'),
(2, 'Juan Jose', 'Mamani', '8522145LP', '1989-08-02', 'M', '9825986', 'opo89@gmail.com', '2019-12-18'),
(3, 'Jhon', 'Mamani', '8522145LP', '1989-08-02', 'M', '9825986', 'opo89@gmail.com', '2019-12-19'),
(4, 'Donald', 'Mamani', '8522145LP', '1989-08-02', 'M', '9825986', 'opo89@gmail.com', '2019-12-31'),
(5, 'Juana', 'Paz', '542525LP', '1999-08-02', 'F', '78763833', 'opo90@gmail.com', '2020-01-08'),
(6, 'Maria', 'Condori', '5185185SCZ', '1998-09-02', 'F', '78736833', 'opo91@gmail.com', '2020-02-11'),
(7, 'Jose', 'Lake', '988985LP', '2001-08-02', 'M', '78763833', 'opo92@gmail.com', '2020-03-26'),
(8, 'Juankd', 'Mamanidfd', '8522145LP', '1989-08-02', 'M', '9825986', 'opo89@gmail.com', '2020-03-20'),
(9, 'Juanakdf', 'Pazdfd', '542525LP', '1999-01-02', 'F', '78763833', 'opo95@gmail.com', '2019-12-28'),
(10, 'Mariaasd', 'Condoriasd', '5185185SCZ', '1998-05-02', 'F', '78736833', 'opo965@gmail.com', '2020-10-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `idturno` int(11) NOT NULL,
  `horario` varchar(10) NOT NULL,
  `descripcion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`idturno`, `horario`, `descripcion`) VALUES
(1, 'Mañana', '**'),
(2, 'Tarde', '-'),
(3, 'Noche', '-'),
(7, 'notif', '22'),
(8, '1', '2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`idalumno`);

--
-- Indices de la tabla `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`idaula`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`idcurso`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`idmateria`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`idprofesor`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`idturno`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `idalumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `aula`
--
ALTER TABLE `aula`
  MODIFY `idaula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `idcurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `idmateria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `idprofesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `idturno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
