<?php
	include_once('../config.php');
	include '../plantillas/head.php';
	if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
		extract($_REQUEST);
		if($idalumno==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=al');
			exit;
		}elseif($idprofesor==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=pr');
			exit;
		}elseif($idturno==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=tu');
			exit;
		}elseif($idaula==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=au');
			exit;
		}elseif($idmateria==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ma');
			exit;
		}elseif($gestion==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ge');
			exit;
		}else{
			$userCount	=	$db->getQueryCount('curso','idcurso');
			if($userCount[0]['total']<20){
				$data	=	array(
								'idalumno'=>$idalumno,
								'idprofesor'=>$idprofesor,
								'idturno'=>$idturno,
								'idaula'=>$idaula,
								'idmateria'=>$idmateria,
								'gestion'=>$gestion,
							);
				$insert	=	$db->insert('curso',$data);
				if($insert){
					header('location:index.php?msg=ras');
					exit;
				}else{
					header('location:index.php?msg=rna');
					exit;
				}
			}else{
				header('location:'.$_SERVER['PHP_SELF'].'?msg=dsd');
				exit;
			}
		}
	}
?>

<div class="container">
	<h1 class="text-primary" style="padding: 20px;">Crear</h1>
	<?php
		if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="al"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Alumno es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="pr"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Profesor es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
			echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Registro añadido satisfactoriamente!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Registro no añadido <strong>Intente otra vez!</strong></div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="dsd"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Por favor, borre el registro e intente otra vez. <strong>Se limito por razones de seguridad!</strong></div>';
		}
	?>
	<div class="card">
		<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Navegacion por Curso</strong> <a href="index.php?msg=rnu" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Volver</a></div>
		<div class="card-body">
			<div class="col-sm-6">
				<?php include './form.php';?>
			</div>
		</div>
	</div>
</div>
<?php include '../plantillas/foot.php';?>