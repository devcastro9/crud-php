<!-- editable select -->
<script async src="http://rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
<link href="http://rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">
<!-- editable select -->
<?php
    $materialist = $db->getAllRecords('materia','idmateria, materia','','ORDER BY materia');
?>
<form method="post">
    <div class="form-group">
        <label>Alumno:</label>
        <input type="text" name="idalumno" id="idalumno" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['idalumno'] : ""; ?>" placeholder="Seleccione item" required>
    </div>
    <div class="form-group">
        <label>Profesor:</label>
        <input type="text" name="idprofesor" id="idprofesor" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['idprofesor'] : ""; ?>" placeholder="Seleccione item" required>
    </div>
    <div class="form-group">
        <label>Turno:</label>
        <input type="text" name="idturno" id="idturno" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['idturno'] : ""; ?>" placeholder="Seleccione item" required>
    </div>
    <div class="form-group">
        <label>Aula:</label>
        <input type="text" name="idaula" id="idaula" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['idaula'] : ""; ?>" placeholder="Seleccione item" required>
    </div>
    <div class="form-group">
        <label>Materia:</label>
        <select class="form-control" name="idmateria" id="idmateria">
            <?php
                $selec = '';
                foreach ($materialist as $rowm) {
                    $selec = (isset($_REQUEST['editId']) and $_REQUEST['editId']!="" and $row[0]['idmateria']==$rowm['idmateria'])? 'selected="selected"' : "";
                    echo '<option value="'.$rowm['idmateria'].'" '.$selec.' >'.$rowm['materia'].'</option>';
                }
            ?>
        </select>
        <br>
    </div>
    <div class="form-group">
        <label>Gestion:</label>
        <input type="number" name="gestion" id="gestion" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['gestion'] : ""; ?>" placeholder="Ingrese gestion" required>
    </div>
    <div class="form-group">
        <?php   if(isset($_REQUEST['editId']) and $_REQUEST['editId']!=""){ ?>
            <input type="hidden" name="editId" id="editId" value="<?php echo $_REQUEST['editId']?>">
        <?php } ?>
        <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i> Aceptar</button>
    </div>
</form>
<!-- <script>
    $(document).ready(function () {
        $('#idmateria').keyup(function () { 
            var query = $(this).val();
            if (query != '') {
                /* $('#materialist').show(); */
                $.ajax({
                    url: "search.php",
                    method:"POST",
                    data: {query:query},
                    success: function(data) {
                        $('#idmateria').fadeIn();
                        /* $('#materialist').html(data); */
                        $('#idmaterialist').html(data);
                    }
                });
            } else {
                /* $('#materialist').hide(); */
            }
        });
        $(document).on('click','li', function () {
            $('#idmateria').val($(this).text());
            $('#idmateria').fadeOut();
        });
    });
</script> -->