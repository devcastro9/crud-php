<?php
	include_once('../config.php');
	include '../plantillas/head.php';
	
	if(isset($_REQUEST['editId']) and $_REQUEST['editId']!=""){
		$row	=	$db->getAllRecords('turno','*',' AND idturno="'.$_REQUEST['editId'].'"');
	}
	if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
		extract($_REQUEST);
		if($horario==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=hor&editId='.$_REQUEST['editId']);
			exit;
		}elseif($descripcion==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=des&editId='.$_REQUEST['editId']);
			exit;
		}
		$data	=	array(
						'horario'=>$horario,
						'descripcion'=>$descripcion,
						);
		$update	=	$db->update('turno',$data,array('idturno'=>$editId));
		if($update){
			header('location: index.php?msg=ras');
			exit;
		}else{
			header('location: index.php?msg=rnu');
			exit;
		}
	}
?>

<div class="container">
	<h1 class="text-primary" style="padding: 20px;">Editar</h1>
	<?php
		if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="hor"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Horario es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="des"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Descripcion es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
			echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Registro añadido satisfactoriamente!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Registro no añadido <strong>Intente otra vez!</strong></div>';
		}
	?>
	<div class="card">
		<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Navegacion por Turno</strong> <a href="index.php?msg=rnu" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Volver</a></div>
		<div class="card-body">
			<div class="col-sm-6">
				<?php include './form.php';?>
			</div>
		</div>
	</div>
</div>
<?php include '../plantillas/foot.php';?>