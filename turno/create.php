<?php
	include_once('../config.php');
	include '../plantillas/head.php';
	if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
		extract($_REQUEST);
		if($horario==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=hor');
			exit;
		}elseif($descripcion==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=des');
			exit;
		}else{
			$userCount	=	$db->getQueryCount('turno','idturno');
			if($userCount[0]['total']<20){
				$data	=	array(
								'horario'=>$horario,
								'descripcion'=>$descripcion,
							);
				$insert	=	$db->insert('turno',$data);
				if($insert){
					header('location:index.php?msg=ras');
					exit;
				}else{
					header('location:index.php?msg=rna');
					exit;
				}
			}else{
				header('location:'.$_SERVER['PHP_SELF'].'?msg=dsd');
				exit;
			}
		}
	}
?>

<div class="container">
	<h1 class="text-primary" style="padding: 20px;">Crear</h1>
	<?php
		if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="hor"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Horario es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="des"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Descripcion es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
			echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Registro añadido satisfactoriamente!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Registro no añadido <strong>Intente otra vez!</strong></div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="dsd"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Por favor, borre el registro e intente otra vez. <strong>Se limito por razones de seguridad!</strong></div>';
		}
	?>
	<div class="card">
		<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Navegacion por Turno</strong> <a href="index.php?msg=rnu" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Volver</a></div>
		<div class="card-body">
			<div class="col-sm-6">
				<?php include './form.php';?>
			</div>
		</div>
	</div>
</div>
<?php include '../plantillas/foot.php';?>