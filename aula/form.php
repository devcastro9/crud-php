<form method="post">
    <div class="form-group">
        <label>Descripcion:</label>
        <input type="text" name="descripcion" id="descripcion" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['descripcion'] : ""; ?>" placeholder="Ingrese descripcion" required>
    </div>
    <div class="form-group">
        <label>Codigo:</label>
        <input type="text" name="codigo" id="codigo" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['codigo'] : ""; ?>" placeholder="Ingrese codigo" required>
    </div>
    <div class="form-group">
        <?php   if(isset($_REQUEST['editId']) and $_REQUEST['editId']!=""){ ?>
            <input type="hidden" name="editId" id="editId" value="<?php echo $_REQUEST['editId']?>">
        <?php } ?>
        <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i> Aceptar</button>
    </div>
</form>