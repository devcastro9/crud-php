<?php
    include_once('../config.php');
    include '../plantillas/head.php';

    $condition	=	'';
    if(isset($_REQUEST['descripcion']) and $_REQUEST['descripcion']!=""){
        $condition	.=	' AND descripcion LIKE "%'.$_REQUEST['descripcion'].'%" ';
    }
    if(isset($_REQUEST['codigo']) and $_REQUEST['codigo']!=""){
        $condition	.=	' AND codigo LIKE "%'.$_REQUEST['codigo'].'%" ';
    }
    $userData	=	$db->getAllRecords('aula','*',$condition,'ORDER BY idaula');
?>

<div class="container-fluid">
    <h1 class="text-primary" style="padding: 20px;">Aula</h1>
    <div class="card">
        <div class="card-header"><i class='fas fa-globe'></i> <strong>Navegación por Aula</strong> <a href="create.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-plus-circle"></i> Crear</a></div>
        <div class="card-body">
            <?php
                if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rds"){
                    echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> ¡Registro borrado satisfactoriamente!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
                    echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> ¡Registro actualizado satisfactoriamente!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rnu"){
                    echo	'<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> ¡No cambiaste nada!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
                    echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Hay algo mal. <strong>¡Inténtalo de nuevo!</strong></div>';
                }
            ?>
            <div class="col-sm">
                <h5 class="card-title"><i class="fa fa-fw fa-search"></i> Buscar aulas:</h5>
                <form method="get">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Descripcion:</label>
                                <input type="text" name="descripcion" id="descripcion" class="form-control" value="<?php echo isset($_REQUEST['descripcion'])?$_REQUEST['descripcion']:''?>" placeholder="Ingrese descripcion...">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Codigo:</label>
                                <input type="text" name="codigo" id="codigo" class="form-control" value="<?php echo isset($_REQUEST['codigo'])?$_REQUEST['codigo']:''?>" placeholder="Ingrese codigo...">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div>
                                    <button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Buscar</button>
                                    &nbsp;
                                    <a href="<?php echo $_SERVER['PHP_SELF'];?>" class="btn btn-danger"><i class="fa fa-fw fa-sync"></i> Borrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</div>
    <br>
    <div class="container">
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="bg-primary text-white">
                    <th class="text-center">Id Aula</th>
                    <th class="text-center">Descripcion</th>
                    <th class="text-center">Codigo</th>
                    <th class="text-center">Accion</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $s	=	'';
                foreach($userData as $val){
                    $s++;
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $val['descripcion'];?></td>
                    <td><?php echo $val['codigo'];?></td>
                    <td align="center">
                        <a href="edit.php?editId=<?php echo $val['idaula'];?>" class="text-primary"><i class="fa fa-fw fa-edit"></i> Editar</a> | 
                        <a href="delete.php?delId=<?php echo $val['idaula'];?>" class="text-danger" onClick="return confirm('¿Desea eliminar este registro?');"><i class="fa fa-fw fa-trash"></i> Borrar</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div>

<?php include '../plantillas/foot.php';?>