<!doctype html>
<html lang="es-ES">
  <head>
    <title>Seguimiento Academico</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Icon -->
    <link href="/crud-php/favicon.ico" rel="icon" type="image/png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-dark bg-dark sticky-top">
        <a class="navbar-brand" href="/crud-php">Seguimiento Academico</a>
    </nav>
    <!-- para funcionar en otros entornos, cambiar: '/p_crud' por '/' -->