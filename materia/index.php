<?php
    include_once('../config.php');
    include '../plantillas/head.php';

    $condition	=	'';
    if(isset($_REQUEST['materia']) and $_REQUEST['materia']!=""){
        $condition	.=	' AND materia LIKE "%'.$_REQUEST['materia'].'%" ';
    }
    if(isset($_REQUEST['codigomateria']) and $_REQUEST['codigomateria']!=""){
        $condition	.=	' AND codigomateria LIKE "%'.$_REQUEST['codigomateria'].'%" ';
    }
    $userData	=	$db->getAllRecords('materia','*',$condition,'ORDER BY idmateria');  
?>

<div class="container-fluid">
    <h1 class="text-primary" style="padding: 20px;">Materia</h1>
    <div class="card">
        <div class="card-header"><i class='fas fa-globe'></i> <strong>Navegación por materia</strong> <a href="create.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-plus-circle"></i> Crear</a></div>
        <div class="card-body">
            <?php
                if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rds"){
                    echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> ¡Registro borrado satisfactoriamente!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
                    echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> ¡Registro actualizado satisfactoriamente!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rnu"){
                    echo	'<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> ¡No cambiaste nada!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
                    echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Hay algo mal. <strong>¡Inténtalo de nuevo!</strong></div>';
                }
            ?>
            <div class="col-sm">
                <h5 class="card-title"><i class="fa fa-fw fa-search"></i> Buscar materia:</h5>
                <form method="get">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Materia:</label>
                                <input type="text" name="materia" id="materia" class="form-control" value="<?php echo isset($_REQUEST['materia'])?$_REQUEST['materia']:''?>" placeholder="Ingrese materia...">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Codigo de materia:</label>
                                <input type="text" name="codigomateria" id="codigomateria" class="form-control" value="<?php echo isset($_REQUEST['codigomateria'])?$_REQUEST['codigomateria']:''?>" placeholder="Ingrese codigo...">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div>
                                    <button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Buscar</button>
                                    &nbsp;
                                    <a href="<?php echo $_SERVER['PHP_SELF'];?>" class="btn btn-danger"><i class="fa fa-fw fa-sync"></i> Borrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</div>
    <br>
    <div class="container">
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="bg-primary text-white">
                    <th class="text-center">Id Materia</th>
                    <th class="text-center">Materia</th>
                    <th class="text-center">Codigo de materia</th>
                    <th class="text-center">Accion</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $s	=	'';
                foreach($userData as $val){
                    $s++;
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $val['materia'];?></td>
                    <td><?php echo $val['codigomateria'];?></td>
                    <td align="center">
                        <a href="edit.php?editId=<?php echo $val['idmateria'];?>" class="text-primary"><i class="fa fa-fw fa-edit"></i> Editar</a> | 
                        <a href="delete.php?delId=<?php echo $val['idmateria'];?>" class="text-danger" onClick="return confirm('¿Desea eliminar este registro?');"><i class="fa fa-fw fa-trash"></i> Borrar</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div>

<?php include '../plantillas/foot.php';?>