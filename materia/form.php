<form method="post">
    <div class="form-group">
        <label>Materia:</label>
        <input type="text" name="materia" id="materia" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['materia'] : ""; ?>" placeholder="Ingrese materia" required>
    </div>
    <div class="form-group">
        <label>Codigo de materia:</label>
        <input type="text" name="codigomateria" id="codigomateria" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['codigomateria'] : ""; ?>" placeholder="Ingrese codigo" required>
    </div>
    <div class="form-group">
        <?php   if(isset($_REQUEST['editId']) and $_REQUEST['editId']!=""){ ?>
            <input type="hidden" name="editId" id="editId" value="<?php echo $_REQUEST['editId']?>">
        <?php } ?>
        <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i> Aceptar</button>
    </div>
</form>