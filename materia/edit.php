<?php
	include_once('../config.php');
	include '../plantillas/head.php';
	
	if(isset($_REQUEST['editId']) and $_REQUEST['editId']!=""){
		$row	=	$db->getAllRecords('materia','*',' AND idmateria="'.$_REQUEST['editId'].'"');
	}
	if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
		extract($_REQUEST);
		if($materia==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ma&editId='.$_REQUEST['editId']);
			exit;
		}elseif($codigomateria==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=cm&editId='.$_REQUEST['editId']);
			exit;
		}
		$data	=	array(
						'materia'=>$materia,
						'codigomateria'=>$codigomateria,
						);
		$update	=	$db->update('materia',$data,array('idmateria'=>$editId));
		if($update){
			header('location: index.php?msg=ras');
			exit;
		}else{
			header('location: index.php?msg=rnu');
			exit;
		}
	}
?>

<div class="container">
	<h1 class="text-primary" style="padding: 20px;">Editar</h1>
	<?php
		if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ma"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Materia es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="cm"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Codigo de materia es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
			echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Registro añadido satisfactoriamente!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Registro no añadido <strong>Intente otra vez!</strong></div>';
		}
	?>
	<div class="card">
		<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Navegacion por Materia</strong> <a href="index.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Volver</a></div>
		<div class="card-body">
			<div class="col-sm-6">
				<?php include './form.php';?>
			</div>
		</div>
	</div>
</div>
<?php include '../plantillas/foot.php';?>