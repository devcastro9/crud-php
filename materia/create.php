<?php
	include_once('../config.php');
	include '../plantillas/head.php';
	if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
		extract($_REQUEST);
		if($materia==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ma');
			exit;
		}elseif($codigomateria==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=cm');
			exit;
		}else{
			$userCount	=	$db->getQueryCount('materia','idmateria');
			if($userCount[0]['total']<20){
				$data	=	array(
								'materia'=>$materia,
								'codigomateria'=>$codigomateria,
							);
				$insert	=	$db->insert('materia',$data);
				if($insert){
					header('location:index.php?msg=ras');
					exit;
				}else{
					header('location:index.php?msg=rna');
					exit;
				}
			}else{
				header('location:'.$_SERVER['PHP_SELF'].'?msg=dsd');
				exit;
			}
		}
	}
?>

<div class="container">
	<h1 class="text-primary" style="padding: 20px;">Crear</h1>
	<?php
		if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ma"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Materia es requerida!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="cm"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Codigo de materia es requerida!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
			echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Registro añadido satisfactoriamente</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Record not added <strong>Please try again!</strong></div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="dsd"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Please delete a user and then try again <strong>We set limit for security reasons!</strong></div>';
		}
	?>
	<div class="card">
		<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Navegacion por materia</strong> <a href="index.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Volver</a></div>
		<div class="card-body">
			<div class="col-sm-6">
				<?php include './form.php';?>
			</div>
		</div>
	</div>
</div>
<?php include '../plantillas/foot.php';?>