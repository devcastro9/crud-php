# crud-php

## Proyecto CRUD con PHP, MySql y Bootstrap.

Este proyecto fue hecho con XAMPP 7.3.12 en el sistema operativo Debian 10 Buster GNU/Linux.

## ¿Como iniciar el proyecto?

1. Primero se debe realizar el clonado del repositorio en la carpeta htdocs:

    `git clone https://gitlab.com/orlandocastro/crud-php.git`

2. Para iniciar XAMPP en Debian:

    `sudo /opt/lampp/manager-linux-x64.run`

    (Posteriormente iniciar Apache y MySql)

3. Finalmente iniciar el navegador en:
    
    `localhost/crud-php`

## Base de datos

El backup de la base de datos para importar con phpmyadmin es:

    `seguimiento_academico.sql`
