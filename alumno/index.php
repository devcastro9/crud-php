<?php
    include_once('../config.php');
    include '../plantillas/head.php';

    $condition	=	'';
    if(isset($_REQUEST['nombre']) and $_REQUEST['nombre']!=""){
        $condition	.=	' AND nombre LIKE "%'.$_REQUEST['nombre'].'%" ';
    }
    if(isset($_REQUEST['apellido']) and $_REQUEST['apellido']!=""){
        $condition	.=	' AND apellido LIKE "%'.$_REQUEST['apellido'].'%" ';
    }
    if(isset($_REQUEST['ci']) and $_REQUEST['ci']!=""){
        $condition	.=	' AND ci LIKE "%'.$_REQUEST['ci'].'%" ';
    }
    $userData	=	$db->getAllRecords('alumno','*',$condition,'ORDER BY idalumno');
?>

<div class="container-fluid">
    <h1 class="text-primary" style="padding: 20px;">Alumno</h1>
    <div class="card">
        <div class="card-header"><i class='fas fa-globe'></i> <strong>Navegación por Alumno</strong> <a href="create.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-plus-circle"></i> Crear</a></div>
        <div class="card-body">
            <?php
                if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rds"){
                    echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> ¡Registro borrado satisfactoriamente!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
                    echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> ¡Registro actualizado satisfactoriamente!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rnu"){
                    echo	'<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> ¡No cambiaste nada!</div>';
                }elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
                    echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Hay algo mal. <strong>¡Inténtalo de nuevo!</strong></div>';
                }
            ?>
            <div class="col-sm">
                <h5 class="card-title"><i class="fa fa-fw fa-search"></i> Buscar alumno:</h5>
                <form method="get">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo isset($_REQUEST['nombre'])?$_REQUEST['nombre']:''?>" placeholder="Ingrese nombre...">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Apellido:</label>
                                <input type="text" name="apellido" id="apellido" class="form-control" value="<?php echo isset($_REQUEST['apellido'])?$_REQUEST['apellido']:''?>" placeholder="Ingrese apellido...">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>CI:</label>
                                <input type="text" name="ci" id="ci" class="form-control" value="<?php echo isset($_REQUEST['ci'])?$_REQUEST['ci']:''?>" placeholder="Ingrese ci...">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div>
                                    <button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Buscar</button>
                                    &nbsp;
                                    <a href="<?php echo $_SERVER['PHP_SELF'];?>" class="btn btn-danger"><i class="fa fa-fw fa-sync"></i> Borrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</div>
    <br>
    <div class="container">
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="bg-primary text-white">
                    <th class="text-center">Id Alumno</th>
                    <th class="text-center">Nombre y Ap.</th>
                    <th class="text-center">CI</th>
                    <th class="text-center">Pagos al dia</th>
                    <th class="text-center">Accion</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $s	=	'';
                foreach($userData as $val){
                    $s++;
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $val['nombre'].' '.$val['apellido'];?></td>
                    <td><?php echo $val['ci'];?></td>
                    <td><?php echo $val['pagosaldia'] == "S"? "Si" : "No";?></td>
                    <td align="center">
                        <a href="edit.php?editId=<?php echo $val['idalumno'];?>" class="text-primary"><i class="fa fa-fw fa-edit"></i> Editar</a> | 
                        <a href="delete.php?delId=<?php echo $val['idalumno'];?>" class="text-danger" onClick="return confirm('¿Desea eliminar este registro?');"><i class="fa fa-fw fa-trash"></i> Borrar</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div>

<?php include '../plantillas/foot.php';?>