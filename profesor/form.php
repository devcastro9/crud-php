<form method="post">
    <div class="form-group">
        <label>Nombre:</label>
        <input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['nombre'] : ""; ?>" placeholder="Ingrese nombre" required>
    </div>
    <div class="form-group">
        <label>Apellido:</label>
        <input type="text" name="apellido" id="apellido" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['apellido'] : ""; ?>" placeholder="Ingrese apellido" required>
    </div>
    <div class="form-group">
        <label>CI:</label>
        <input type="text" name="ci" id="ci" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['ci'] : ""; ?>" placeholder="Ingrese ci" required>
    </div>
    <div class="form-group">
        <label>Fecha de nacimiento:</label>
        <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['fecha_nacimiento'] : ""; ?>" placeholder="Ingrese fecha nacimiento" required>
    </div>
    <div class="form-group">
        <label>Genero:</label>
        <select name="genero" id="genero" class="form-control">
            <option value="F" <?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="" and $row[0]['genero']=="F")? 'selected="selected"' : ""; ?> >Femenino</option>
            <option value="M" <?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="" and $row[0]['genero']=="M")? 'selected="selected"' : ""; ?> >Masculino</option>
            <option value="O" <?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="" and $row[0]['genero']=="O")? 'selected="selected"' : ""; ?> >Otro</option>
        </select>
    </div>
    <div class="form-group">
        <label>Celular:</label>
        <input type="text" name="celular" id="celular" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['celular'] : ""; ?>" placeholder="Ingrese celular" required>
    </div>
    <div class="form-group">
        <label>Correo electronico:</label>
        <input type="email" name="correo_electronico" id="correo_electronico" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['correo_electronico'] : ""; ?>" placeholder="Ingrese correo electronico" required>
    </div>
    <div class="form-group">
        <label>Fecha de contrato:</label>
        <input type="date" name="fecha_contrato" id="fecha_contrato" class="form-control" value="<?php echo (isset($_REQUEST['editId']) and $_REQUEST['editId']!="")? $row[0]['fecha_contrato'] : ""; ?>" placeholder="Ingrese fecha contrato" required>
    </div>
    <div class="form-group">
        <?php   if(isset($_REQUEST['editId']) and $_REQUEST['editId']!=""){ ?>
            <input type="hidden" name="editId" id="editId" value="<?php echo $_REQUEST['editId']?>">
        <?php } ?>
        <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i> Aceptar</button>
    </div>
</form>