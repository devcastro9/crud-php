<?php
	include_once('../config.php');
	include '../plantillas/head.php';
	
	if(isset($_REQUEST['editId']) and $_REQUEST['editId']!=""){
		$row	=	$db->getAllRecords('profesor','*',' AND idprofesor="'.$_REQUEST['editId'].'"');
	}
	if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
		extract($_REQUEST);
		if($nombre==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=nm&editId='.$_REQUEST['editId']);
			exit;
		}elseif($apellido==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ap&editId='.$_REQUEST['editId']);
			exit;
		}elseif($ci==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ci&editId='.$_REQUEST['editId']);
			exit;
		}elseif($fecha_nacimiento==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=nac&editId='.$_REQUEST['editId']);
			exit;
		}elseif($genero==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ge&editId='.$_REQUEST['editId']);
			exit;
		}elseif($celular==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=cel&editId='.$_REQUEST['editId']);
			exit;
		}elseif($correo_electronico==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=ce&editId='.$_REQUEST['editId']);
			exit;
		}elseif($fecha_contrato==""){
			header('location:'.$_SERVER['PHP_SELF'].'?msg=fc&editId='.$_REQUEST['editId']);
			exit;
		}
		$data	=	array(
						'nombre'=>$nombre,
						'apellido'=>$apellido,
						'ci'=>$ci,
						'fecha_nacimiento'=>$fecha_nacimiento,
						'genero'=>$genero,
						'celular'=>$celular,
						'correo_electronico'=>$correo_electronico,
						'fecha_contrato'=>$fecha_contrato,
						);
		$update	=	$db->update('profesor',$data,array('idprofesor'=>$editId));
		if($update){
			header('location: index.php?msg=ras');
			exit;
		}else{
			header('location: index.php?msg=rnu');
			exit;
		}
	}
?>

<div class="container">
	<h1 class="text-primary" style="padding: 20px;">Editar</h1>
	<?php
		if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="nm"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Nombre es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ap"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Apellido es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ci"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> CI es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="nac"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Fecha de nacimiento es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ge"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Genero es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="cel"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Celular es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ce"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Correo electronico es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="fc"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Fecha de contrato es requerido!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
			echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Registro añadido satisfactoriamente!</div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
			echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Registro no añadido <strong>Intente otra vez!</strong></div>';
		}
	?>
	<div class="card">
		<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Navegacion por Profesor</strong> <a href="index.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Volver</a></div>
		<div class="card-body">
			<div class="col-sm-6">
				<?php include './form.php';?>
			</div>
		</div>
	</div>
</div>
<?php include '../plantillas/foot.php';?>